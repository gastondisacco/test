# Coding Challenge - installation instructions

It is recommended to clone this repo into a Homestead VM (the official Vagrant box for Laravel / Lumen that includes all tools and goodies you may need).

Once cloned, run `composer install` at project root, from the terminal, to download all dependencies listed on the composer.json file.

Copy or rename the .env.example file into .env and fill out your APP_KEY (there is no Artisan command to generate the key in Lumen so you should enter it by yourself, just a random 32 character long string) and setup database information.

Artisan is the CLI included with Laravel / Lumen.

Run `php artisan jwt:secret`, this will generate the JWT_SECRET key on your .env file that will be used in the user authentication.

Run `run php artisan migrate:refresh --seed`, this will load the database with structure and records, records are pulled from `database/data/db.json` and users are created with the Faker library, all with Password1 as password.

You can import this Postman collection https://www.getpostman.com/collections/43bb2f5b45ea46435955, it has all the APIs developed loaded.

Before starting playing with the APIs, please create a new environment in Postman with two variables, `LumenURL` and `token`, this will make it a lot easier, you just have to define the URL of your local Lumen app and the Collection will use it. Keep the token variable empty.

First steps: In Postman, open the Login call under Authorization from the collection provided, send the call and it will obtain the access_token needed in all the secured APIs, this will be stored in the token env variable so you can forget about it.

There are endpoints to retrieve collections of properties, markets, and countries.

There are also endpoints to retrieve items from properties, markets and, countries.


For properties, you can also create new records, update and delete existing ones.




