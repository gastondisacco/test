<?php

namespace App;

use Neomerx\JsonApi\Schema\SchemaProvider;

class PropertySchema extends SchemaProvider
{
    protected $resourceType = 'properties';
    
    protected $selfSubUrl   = '/properties';

    public function getId($property)
    {
        /** @var Property $property */
        return $property->id;
    }
    /*
    'name',
        'desks',
        'Sf',
        'address1',
        'address2',
        'city',
        'state',
        'postalCode',
        'latitude',
        'longitude',
        'countryId',
        'regionalCategory',
        'marketId',
        'submarketId',
        'locationId',
    */
    public function getAttributes($property)
    {
        /** @var Property $property */
        return [
            'name' => $property->name,
            'desks' => $property->desks,
            'Sf' => $property->Sf,
            'address1' => $property->address1,
            'address2' => $property->address2,
            'city' => $property->state,
            'postalCode' => $property->postalCode,
            'latitude' => $property->latitude,
            'longitude' => $property->longitude,
            'countryId' => $property->countryId,
            'regionalCategory' => $property->regionalCategory,
            'marketId' => $property->marketId,
            'submarketId' => $property->submarketId,
            'locationId' => $property->locationId
        ];
    }

    public function getRelationships($property, $isPrimary, array $includeList)
    {
        /** @var Property $property */
        return [
            'country' => [self::DATA => $property->country],
            'market' => [self::DATA => $property->market],
            'submarket' => [self::DATA => $property->submarket],
        ];
    }
}