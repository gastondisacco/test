<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Market extends Model
{

    public $timestamps = false;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'market',
    ];

    function properties()
    {
        return $this->hasMany('App\Property', 'marketId', 'id');
    }
}
