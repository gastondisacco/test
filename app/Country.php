<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{

    protected $table = 'countries';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'iso2',
        'shortName',
        'longName',
        'iso3',
        'numCode',
    ];

    function properties()
    {
        return $this->hasMany('App\Property', 'countryId', 'id');
    }
}
