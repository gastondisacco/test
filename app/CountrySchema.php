<?php

namespace App;

use Neomerx\JsonApi\Schema\SchemaProvider;

class CountrySchema extends SchemaProvider
{
    protected $resourceType = 'countries';
    
    protected $selfSubUrl   = '/countries';

    public function getId($country)
    {
        /** @var Country $country */
        return $country->id;
    }

    public function getAttributes($country)
    {
        /** @var Country $country */
        return [
            'iso2' => $country->iso2,
            'shortName' => $country->shortName,
            'longName' => $country->longName,
            'iso3' => $country->iso3,
            'numCode' => $country->numCode
        ];
    }
}