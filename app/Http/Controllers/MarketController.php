<?php

namespace App\Http\Controllers;

use \App\Market;
use Illuminate\Http\Request;
use RealPage\JsonApi\EncoderService;
use RealPage\JsonApi\Validation\ValidatesRequests;

use Neomerx\JsonApi\Encoder\Encoder;
use Neomerx\JsonApi\Encoder\EncoderOptions;

class MarketController extends Controller
{
    /**
     * Create a new Market instance.
     *
     * @return void
     */
    public function __construct(EncoderService $encoder)
    {
        $this->middleware('auth:api');
        $this->encoder = $encoder->getEncoder();
    }

    function index() {
        $markets = Market::orderBy('name')->get();
        return response($this->encoder->encodeData($markets))->header('Content-Type', 'application/vnd.api+json');
    }

    function get($id) {
        $market = Market::findOrFail($id);
        return response($this->encoder->encodeData($market))->header('Content-Type', 'application/vnd.api+json');
    }
}
