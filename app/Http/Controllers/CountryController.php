<?php

namespace App\Http\Controllers;

use \App\Country;
use Illuminate\Http\Request;
use RealPage\JsonApi\EncoderService;
use RealPage\JsonApi\Validation\ValidatesRequests;

use Neomerx\JsonApi\Encoder\Encoder;
use Neomerx\JsonApi\Encoder\EncoderOptions;

class CountryController extends Controller
{
    /**
     * Create a new CountryController instance.
     *
     * @return void
     */
    public function __construct(EncoderService $encoder)
    {
        $this->middleware('auth:api');
        $this->encoder = $encoder->getEncoder();
    }

    function index() {
        $countries = Country::orderBy('shortName')->get();
        return response($this->encoder->encodeData($countries))->header('Content-Type', 'application/vnd.api+json');
    }

    function get($id) {
        $country = Country::findOrFail($id);
        return response($this->encoder->encodeData($country))->header('Content-Type', 'application/vnd.api+json');
    }
}
