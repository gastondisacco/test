<?php

namespace App\Http\Controllers;

use App\Property;
use Illuminate\Http\Request;
use RealPage\JsonApi\EncoderService;
use RealPage\JsonApi\Validation\ValidatesRequests;

use Neomerx\JsonApi\Encoder\Encoder;
use Neomerx\JsonApi\Encoder\EncoderOptions;

class PropertyController extends Controller
{

    protected $encoder;

    /**
     * Create a new Property instance.
     *
     * @return void
     */
    public function __construct(EncoderService $encoder)
    {
        $this->middleware('auth:api');
        $this->middleware('json-api.enforce-media-type', ['except' => ['store', 'update']]);
        $this->encoder = $encoder->getEncoder();
    }

    function index() {
        $properties = Property::orderBy('name')->get();
        return response($this->encoder->encodeData($properties))->header('Content-Type', 'application/vnd.api+json');
    }

    function get($id) {
        $property = Property::with('market', 'submarket', 'country')->findOrFail($id);
        return response($this->encoder->encodeData($property))->header('Content-Type', 'application/vnd.api+json');
    }

    function store(Request $request) {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'desks' => 'nullable|integer',
            'Sf' => 'nullable|integer',
            'address1' => 'required|string|max:255',
            'address2' => 'nullable|string|max:255',
            'city' => 'required|string|max:255',
            'state' => 'required|string|max:255',
            'postalCode' => 'nullable|max:255',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric',
            'countryId' => 'required|exists:countries,id',
            'regionalCategory' => 'nullable|integer',
            'marketId' => 'required|exists:markets,id',
            'submarketId' => 'required|exists:markets,id',
            'locationId' => 'integer',
        ]);
        $property = Property::create($request->all());
        return response($property)->header('Content-Type', 'application/vnd.api+json');
    }

    function update(Request $request, $id) {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'desks' => 'nullable|integer',
            'Sf' => 'nullable|integer',
            'address1' => 'required|string|max:255',
            'address2' => 'nullable|string|max:255',
            'city' => 'required|string|max:255',
            'state' => 'required|string|max:255',
            'postalCode' => 'nullable|max:255',
            'latitude' => 'required|numeric',
            'longitude' => 'required|numeric',
            'countryId' => 'required|exists:countries,id',
            'regionalCategory' => 'nullable|integer',
            'marketId' => 'required|exists:markets,id',
            'submarketId' => 'required|exists:markets,id',
            'locationId' => 'integer',
        ]);
        $property = Property::findOrFail($id);
        $update = $property->update($request->all());
        return response($property)->header('Content-Type', 'application/vnd.api+json');
    }

    function destroy($id) {
        $property = Property::findOrFail($id);
        $property->delete();
        return response($property)->header('Content-Type', 'application/vnd.api+json');
    }

}
