<?php

namespace App;

use Neomerx\JsonApi\Schema\SchemaProvider;

class MarketSchema extends SchemaProvider
{
    protected $resourceType = 'markets';
    
    protected $selfSubUrl   = '/markets';

    public function getId($market)
    {
        /** @var Market $market */
        return $market->id;
    }
    
    public function getAttributes($market)
    {
        /** @var Market $market */
        return [
            'name' => $market->name,
            'market' => $market->market
        ];
    }
}