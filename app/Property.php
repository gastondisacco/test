<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Property extends Model
{  

    protected $table = 'properties';
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'desks',
        'Sf',
        'address1',
        'address2',
        'city',
        'state',
        'postalCode',
        'latitude',
        'longitude',
        'countryId',
        'regionalCategory',
        'marketId',
        'submarketId',
        'locationId',
    ];

    public function market()
    {
        return $this->belongsTo('App\Market', 'marketId');
    }

    public function submarket()
    {
        return $this->belongsTo('App\Market', 'submarketId');
    }

    function country()
    {
        return $this->belongsTo('App\Country', 'countryId');
    }
}
