<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/


$router->group([
'prefix' => 'auth',
], function ($router) {
    $router->post('login', 'AuthController@login');
    $router->post('logout', 'AuthController@logout');
    $router->post('refresh', 'AuthController@refresh');
    $router->post('me', 'AuthController@me');
});

$router->group([
	'prefix' => 'countries',
	'middleware' => 'json-api.enforce-media-type'
], function ($router) {
	$router->get('/', 'CountryController@index');
	$router->get('{id}', 'CountryController@get');
});

$router->group([
	'prefix' => 'markets',
	'middleware' => 'json-api.enforce-media-type'
], function ($router) {
	$router->get('/', 'MarketController@index');
	$router->get('{id}', 'MarketController@get');
});

$router->group([
	'prefix' => 'properties'
], function ($router) {
	$router->get('/', 'PropertyController@index');
	$router->get('{id}', 'PropertyController@get');
	$router->post('/', 'PropertyController@store');
	$router->put('{id}', 'PropertyController@update');
	$router->delete('{id}', 'PropertyController@destroy');
});

