<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('desks')->nullable();
            $table->integer('Sf')->nullable();
            $table->string('address1');
            $table->string('address2')->nullable();
            $table->string('city');
            $table->string('state');
            $table->string('postalCode')->nullable();
            $table->decimal('latitude', 10, 7);
            $table->decimal('longitude', 10, 7);
            $table->integer('countryId')->unsigned();
            $table->foreign('countryId')->references('id')->on('countries');
            $table->integer('regionalCategory')->nullable();
            $table->integer('marketId')->unsigned();
            $table->foreign('marketId')->references('id')->on('markets');
            $table->integer('submarketId')->unsigned();
            $table->foreign('submarketId')->references('id')->on('markets');
            $table->integer('locationId')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
