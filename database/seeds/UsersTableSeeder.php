<?php

use Illuminate\Database\Seeder;

use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	User::create([
    		'name' => 'Gastón Di Sacco',
    		'email' => 'gastondisacco@gmail.com',
    		'password' => app('hash')->make('Password1'),
    	]);
    	factory(User::class, 9)->create();
    }
}
