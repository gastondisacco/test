<?php

use Illuminate\Database\Seeder;

use App\Country;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$file_path = realpath(__DIR__ . '/../../database/data/db.json');
    	$json = json_decode(file_get_contents($file_path), true);
    	$countries = $json['countries'];
    	foreach ($countries as $country) {
    		Country::create($country);
    	}
    }
}
