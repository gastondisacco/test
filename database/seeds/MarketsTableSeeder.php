<?php

use Illuminate\Database\Seeder;

use App\Market;

class MarketsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $file_path = realpath(__DIR__ . '/../../database/data/db.json');
    	$json = json_decode(file_get_contents($file_path), true);
    	$markets = $json['markets'];
    	foreach ($markets as $market) {
    		Market::create($market);
    	}
    }
}
