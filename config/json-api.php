<?php
return [
    'media-type' => 'application/vnd.api+json',
    'schemas' => [
        App\Property::class => App\PropertySchema::class,
        App\Country::class => App\CountrySchema::class,
        App\Market::class => App\MarketSchema::class,
    ],
    'jsonapi' => false,
    'encoder-options' => [
        'options' => JSON_PRETTY_PRINT,
    ],
    'meta' => [
        'apiVersion' => '1.0',
    ],
];